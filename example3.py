import cv2
import numpy as np

img = cv2.imread('galery/mike.jpg')
kernel = np.ones((5,5),np.uint8)

#Set a color
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
imgBlur = cv2.GaussianBlur(img,(7,7),0)
imgCanny = cv2.Canny(img,100,100)
imgDialation = cv2.dilate(imgCanny, kernel, iterations=1)

imgEroded = cv2.erode(imgDialation, kernel, iterations=1)
# cv2.imshow('My gray pic', imgGray)
# cv2.imshow('My Blur pic', imgBlur)
# cv2.imshow('MyCanny pic', imgCanny)
# cv2.imshow('my Dialation pic', imgDialation)
cv2.imshow('my erode pic', imgEroded)


cv2.waitKey(0)