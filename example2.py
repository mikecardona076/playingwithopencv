import cv2 

cap = cv2.VideoCapture(0) #Give the path of your video to it 
#Even if u want to use your webcam 
cap.set(3,640)
cap.set(4,480)
#Change the brighees
# cap.set(3,300)

while True:
    success, img = cap.read()
    cv2.imshow('My video', img)

    #To break the loop
    if cv2.waitKey(1) and 0xFF  == ord('q'):
        break